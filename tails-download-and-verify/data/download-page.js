(() => {
'use strict';

var
  $ = (s) => typeof s === "string" ? document.querySelector(s) : s,
  $$ = (s) => document.querySelectorAll(s),
  on = (s, evType, listener) => {
    let ee = $$(s);
    if (ee.length) {
      let l = ev => { ev.preventDefault(); ev.stopPropagation(); listener(ev); };
      for (let e of ee) e.addEventListener(evType, l, true);
      return l;
    }
    return null;
  },
  toggle = (s, visible) => {
    if (typeof s === "string") {
      for (let el of $$(s)) toggle(el, visible);
      return;
    }

    if (s) {
      let a = "dave-hide", r = "dave-show";
      if (visible) [a, r] = [r, a];
      let cl = s.classList;
      cl.add(a);
      cl.remove(r);
    }
  },
  reload = () => {
     setTimeout(() => { location.href = location.href; }, 20);
  }
;
{
  let root = document.documentElement;
  console.log(`Init on ${window.location}, ${root}, ${root && root.hasAttribute('data-extension')}`);
  if (root.hasAttribute("data-extension")) location.href = location.href;
  root.dataset.phase = "init";
}
function checkVersion() {

  function compareVersions(v1, v2) {
    if (v1 === v2) return 0;
    let [a, b] = [v1, v2].map(s => String(s).split("."));
     for (let j = 0, len = Math.max(a.length, b.length); j < len; j++) {
       let [x, y] = [a[j], b[j]].map(s => s || "0");
       let [n1, n2] = [x, y].map(s => parseInt(s, 10));
       if (n1 > n2) return 1;
       if (n1 < n2) return -1;
       [x, y] = [x, y].map(s => s.match(/[a-z].*/i));
       if (x || y) {
         if (x && y) return x[0] < y[0] ? -1 : x[0] > y[0] ? 1 : 0;
         return x ? -1 : 1;
       }
     }
     return 0;
  }

  var version = $("#extension-version");
  if (!version) return false;

  let versionCmp = compareVersions(self.options.version, version.textContent);
  let ok = versionCmp >= 0;

  self.port.on("shutdown", () => location.href = location.href);
  document.documentElement.dataset.extension = ok ? "ok" : "old";
  return ok;
}

if (!checkVersion()) {
  document.documentElement.removeAttribute("data-phase");
  return;
}

var verifySuccess = null;

let sizeMiB = size => Math.round(size / 1024 / 1024 * 10) / 10;

let updateBlobView = ({blob: info}) => {
  for(let el of $$('.iso-url'))  el.href = info.url;
  for(let el of $$('.iso-url-text')) el.textContent  = info.url;
  for(let el of $$('.iso-hash')) el.textContent = info.hash;
  for(let el of $$('.download-path')) el.textContent = "???";
  for(let el of $$('.iso-size')) el.textContent = info.size;
  for(let el of $$('.iso-size-KB')) el.textContent = Math.round(info.size / 1024);
  for(let el of $$('.iso-size-MiB')) el.textContent = sizeMiB(info.size);
  for(let el of $$('.iso-version')) el.textContent = info.version;
},

updateDownloadView = ({downloadInfo: info, blob: blob}) => {
  let elapsedSecs = Math.round((Date.now() - info.startTime) / 1000);
  let size = info.size || blob.size || info.totalBytes;
  let currentBytes = info.currentBytes;
  let speed = elapsedSecs > 0 ? Math.round(currentBytes / 1024 / elapsedSecs) : 0;
  let speedKBs = speed || "???";
  let speedKbs = speed ? speed * 8 : "???";
  let downloadedKB =  Math.round(currentBytes / 1024);
  let downloadedMiB = sizeMiB(currentBytes);
  let eta = currentBytes > 0 ? Math.round(elapsedSecs / currentBytes * (size - currentBytes)) : 0;
  let etaMins = Math.floor(eta / 60);
  let progressKnown = info.hasProgress &&  typeof info.progress === "number";
  let progress = progressKnown ? info.progress : 100;

  let broken = info.error || (info.succeeded && currentBytes < info.totalBytes);
  let state = broken ? "failed" : (info.stopped ? (info.succeeded ? "done" : "pause") : "going");
  console.log(`Download state: ${state}`);
  $("#download").dataset.state = state;

  for(let el of $$('.iso-url'))  el.href = info.url;
  for(let el of $$('.iso-url-text')) el.textContent  = info.url;
  for(let el of $$('.download-path')) el.textContent = info.path;
  for(let el of $$('.iso-size-KB')) el.textContent = Math.round(size / 1024);
  for(let el of $$('.iso-size-MiB')) el.textContent = sizeMiB(size);
  for(let el of $$('.eta-secs')) el.textContent = el.parentNode.dataset.value = eta || "???";
  for(let el of $$('.eta-mins')) el.textContent = el.parentNode.dataset.value = etaMins || "???";

  if (currentBytes && speed || broken || info.stopped) {
    for(let el of $$('.speed-Kbs')) el.textContent = el.parentNode.dataset.value = speedKbs;
    for(let el of $$('.speed-KBs')) el.textContent = el.parentNode.dataset.value = speedKBs;
    for(let el of $$('.downloaded-KB')) el.textContent = downloadedKB;
    for(let el of $$('.downloaded-MiB')) el.textContent = downloadedMiB;
  }

  if (progressKnown  || currentBytes > 0) {
    for(let el of $$('#download-progress .progress-bar')) {
      el.setAttribute("aria-value", `${progress}%`);
      el.style.width = `${progress}%`;
    }
    for(let el of $$('#download-progress .progress-label')) el.textContent = progressKnown ? `${info.progress}%` : '???';
  }

  toggle('#download-eta, #download-path', true);
},

updateVerifyView = ({phase, verifyInfo: info, error, failed }) => {
  let done = failed || phase === 'verified';

  for(let el of $$('.verify-progress')) el.textContent = info.progress + "%";
  for(let el of $$('.verify-file-path')) el.textContent = info.path;
  for(let el of $$('.iso-hash')) el.textContent = info.hash;
  for(let el of $$('.iso-computed-hash')) {
    el.textContent = info.computedHash;
    if (done) {
      let cl = el.classList;
      cl.remove("bg-success");
      cl.remove("bg-danger");
      cl.add(failed ? "bg-danger" : "bg-success");
    }
  }

  let failures = info.failures || 0;
  console.log(`Failed verification ${failures}`);
  toggle("#verify-text-state-calculating", !done);
  if (failed) {
    toggle("#verify-text-state-done, #verify-text-success", false);
    toggle("#verify-text-failure", failures === 1);
    toggle("#verify-text-failure-again", failures > 1);
    toggle("#verify-text-state-failed", true);
    let errorEl = $("#verify-error");
    if (errorEl) {
      errorEl.textContent = err;
      toggle(errorEl, true);
    }
  } else {
    verifySuccess = info.path;
    toggle("#verify-text-state-done, #verify-text-success", done);
    toggle("#verify-text-state-failed, #verify-text-failure, #verify-text-failure-again", false);
  }
},
initWidgets = (status) => {
  let useExtension = () =>  {
    toggle("#use-button", false);
    toggle("#use-text", true);
    Download.start();
  };

  toggle("#use-button, #i_have_iso", true);

  // check whether we've just been installed/updated and synchronize UI accordingly
  let loadReason = self.options.loadReason;
  let sinceStartup = Date.now() - self.options.startupTime;
  console.log(`DAVE ${self.options.version} ${loadReason} ${sinceStartup}ms ago.`);
  if (sinceStartup < 10000 && /^(?:install|(?:up|down)grade)$/.test(loadReason)) {
    let id ='upgrade' === loadReason ? 'update' : 'install';
    toggle(`#use, #${id} > *`, false);
    toggle(`#${id}, #${id}-text`, true);
    Download.start();
  } else if (status.downloadInfo) {
    useExtension();
  } else {
    if (!on("#use-button", "click", useExtension)) {
      Download.start(); // if there's no #use-button, we start right away
    }
  }
  on("a.iso-url", "click", e => {
    if (/-retry$/.test(e.target.id)) return;
    Download.cmd('start', { prompt: e.target.title });
  });

  for (let action of ['pause', 'resume', 'cancel']) {
    let cmd = action;
    on(`#download-button-state-${cmd}`, "click", e => {
      if (/\bdownload-again\b/.test(e.target.className)) {
        Download.reset();
      } else {
        Download.cmd(cmd);
      }
    });
  }
  on(".download-again", "click", e => Download.reset());
  on("#download-button-state-retry", "click", e => Download.retry());

  on("#i_have_iso", "click", e => Download.cmd('verify', { prompt: e.target.title }));

  on("#verify-text-success .btn", "click", e => Download.cmd('retry'));

  initWidgets = null;
},
Download = {
  resetting: false,
  start() {

    toggle("#download-eta, #download-path", false);
    toggle("#download", true);
    document.documentElement.dataset.phase = "started";
    this.cmd('init');
  },
  
  reset() {
    this.resetting = true;
    this.cmd("reset");
  },
  
  cmd(cmd, data) {
    console.log(`Sending command Download.${cmd}`);
    self.port.emit("download-control", Object.assign({ cmd, page: document.URL, domain: document.domain }, data));
  },
  update(status) {
    console.log(`Pagemod received ${uneval(status)}`);
    try {
      updateBlobView(status);
    } catch (e) {
      console.log(e);
    }
    if (!document.documentElement) {
      document.addEventListener("DOMContentLoaded", e => Download.start());
      return;
    }


    if (initWidgets) initWidgets(status);


    switch(status.phase) {
      case 'ready':
        if (document.documentElement.dataset.phase === "started") return;
        break;
      case 'started':
        Download.start();
      break;
      case 'downloading':
      case 'downloaded':
        updateDownloadView(status);
        break;
      case 'verifying':
      case 'verified':
        updateVerifyView(status);
        break;
      case 'init':
        if (Download.resetting) {
          reload();
        }
        break;
    }

    document.documentElement.dataset.phase = status.phase;

  }
};

self.port.on("download-status", Download.update);

})();
