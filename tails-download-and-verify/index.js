var self = require('sdk/self');

var downloader = null;
var workers = new Set();

var Config = require("conf.json");

if (Config.pinGlobally) {
  require("./lib/cert-pinner").pinGlobally(Config.pins);
}

function setup(worker) {
  workers.add(worker);
  worker.port.on("download-control", onUI);
  worker.port.on("detach", () => workers.delete(worker));
  if (!downloader) {
    downloader = require("./lib/downloader.js");
    downloader.addListener(notify);
  }
  downloader.pageLoaded(worker.url);
}

function onUI(msg) {
  console.log(uneval(msg));
  switch(msg.cmd) {
    case "start":
      downloader.start(msg.prompt);
    break;
    case "pause":
    case "cancel":
    case "resume":
    case "retry":
    case "reset":
      downloader[msg.cmd].call(downloader, msg.page);
    break;
    case "verify":
      downloader.verify({prompt: msg.prompt});
    break;
    case "reveal":
    downloader.reveal(msg.path);
  }
}

function notify(msg) {
  for (let w of workers.values()) try {
    w.port.emit("download-status", msg);
  } catch (e) {
    console.log(e);
  }
}



exports.main = (opts) => {
  let loadReason = opts.loadReason;
   require('sdk/page-mod').PageMod({
    include: [
      "https://tails.boum.org/*",
    ],
    contentScriptFile: [
      "./download-page.js"
    ],
    attachTo: ["existing", "top"],
    contentScriptWhen: "ready",
    contentScriptOptions: {
      version: self.version,
      loadReason: loadReason,
      startupTime: Date.now()
    },

    onAttach: setup,
  });
};

exports.onUnload = (reason) => {
 if (downloader) downloader.shutdown();
 for (let w of workers.values()) try {
   w.port.emit("shutdown");
 } catch (e) {
   console.log(e);
 }
};
