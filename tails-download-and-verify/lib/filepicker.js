const { Ci, Cc } = require("chrome");

function promptForFile(title, save, defFile) {
  var window = require("sdk/window/utils").getMostRecentBrowserWindow();
  const nsIFilePicker = Ci.nsIFilePicker;

  var fp = Cc["@mozilla.org/filepicker;1"]
           .createInstance(nsIFilePicker);
  fp.init(window, title || "Select a file", nsIFilePicker[`mode${save ? 'Save' : 'Open'}`]);
  fp.appendFilters(nsIFilePicker.filterAll | nsIFilePicker.filterText);
  if (defFile) {
    if (defFile.exists && defFile.exists()) {
      let isDir = defFile.isDirectory();
      fp.displayDirectory = isDir ? defFile : defFile.parent;
      if (!isDir) fp.defaultString = defFile.leafName;
    } else {
      fp.defaultString = defFile.leafName || defFile;
    }
  }
  var rv = fp.show();
  return (rv == nsIFilePicker.returnOK || rv == nsIFilePicker.returnReplace) ? fp.file.path : null;
}

Object.assign(exports, { promptForFile });
