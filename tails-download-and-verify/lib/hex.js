exports.bin2hex = bytesAsString => { let bytes = []; for (let i in bytesAsString) bytes.push(`0${bytesAsString.charCodeAt(i).toString(16)}`.slice(-2)); return bytes.join(''); };
