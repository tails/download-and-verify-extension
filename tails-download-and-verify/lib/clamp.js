exports.clamp = (callback, ms = 500) => {
  var lastTime = Date.now();
  return (...args) => {
     let now = Date.now();
     if (Date.now() - lastTime > ms) {
        callback(...args);
        lastTime = now;
     }
  };
};
