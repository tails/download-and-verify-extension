const { Ci, Cc, Cu } = require("chrome");

Cu.import("resource://gre/modules/Services.jsm");

const NS_BINDING_ABORTED = 0x804b0002;
const DOCUMENT_LOAD_FLAGS = Ci.nsIChannel.LOAD_DOCUMENT_URI | Ci.nsIChannel.LOAD_CALL_CONTENT_SNIFFERS;

function checkCertificate(cert, knownCerts) {
  if (!cert) throw new Error(`No certificate bound to channel (pin: "${known.subjectName}")`);
  if (!(Array.isArray(knownCerts) && knownCerts.length)) throw new Error(`No certification pinning data for ${cert.subjectName}!`);
  let mismatches = [];
  for (let known of knownCerts) {
    let match = true;
    for (let p of Object.keys(known)) {
      if (known[p] !== cert[p]) {
        mismatches.push(`${p}@${known.subjectName} ("${known[p]}" != "${cert[p]}")`);
        match = false;
        break;
      }
    }
    if (match) {
      console.log(`Certificate ${cert.subjectName} matches :)`);
      return;
    }
  }
  throw new Error(`Certificate mismatch: ${cert.subjectName} does not match ${mismatches.join(", ")}!`);
}

function checkChannel(channel, pins, allowUnpinned = false) {

  if (!(channel instanceof Ci.nsIHttpChannel)) throw new Error(`No HTTPS channel for ${channel.name}!`);
  let host = channel.URI.asciiHost;
  if (!(host in pins.domains)) {
    if (allowUnpinned) return;
    throw new Error(`No configured pin for ${host}!`);
  }
  let pin = pins.domains[host];
  if (!(pin.certs || pin.issuers)) return;
  let securityInfo = channel.securityInfo;
  if (!(securityInfo instanceof Ci.nsITransportSecurityInfo)) {
    throw new Error(`No certificate for ${host}!`);
  }
  let cert = channel.securityInfo
    .QueryInterface(Ci.nsISSLStatusProvider).SSLStatus.serverCert;

  console.log(`Checking pins for ${host}...`);

  let certs = names => names.map(name => pins.certs[name]);

  if (pin.certs) {
    checkCertificate(cert, certs(pin.certs));
  }
  if (pin.issuers) {
    let errors = [];
    for (let parent = cert.issuer; parent; parent = parent.issuer) {
      try {
        checkCertificate(parent, certs(pin.issuers));
        return;
      } catch (e) {
        errors.push(e.message);
      }
    }
    throw new Error(errors.length ? errors.join("\n") : `No issuer for certificate "${cert.subjectName}"@${channel.name}!`);
  }
}


function fetch(pins, url, onSuccess, onFail) {
  var req = Cc["@mozilla.org/xmlextras/xmlhttprequest;1"].createInstance();
  req.mozBackgroundRequest = true;
  req.open('GET', url, true);
  req.channel.loadFlags |= Ci.nsIRequest.LOAD_ANONYMOUS;
  req.onreadystatechange = () => {
    if (req.readyState === 2) try {
      checkChannel(req.channel, pins);
    } catch (ex) {
      console.log(ex);
      req.abort();
      onFail(ex);
    }
  };
  req.addEventListener("abort", e => onFail(`Network error: request for ${url} aborted!`), false);
  req.addEventListener("error", e => onFail(`Network error: cannot fetch ${url}!`), false);
  req.addEventListener("load", e => { onSuccess(req.responseText); }, false);
  req.send();
}

var globalPinsListener;
const httpEvents = ["http-on-examine-response"];
function pinGlobally(pins) {
  let events = require("sdk/system/events");
  let globalPins = pins ? Object.assign({}, pins) : null;
  if (!globalPins) {
    if (globalPinsListener) {
      for (let e of httpEvents) events.off(e, globalPinsListener);
      globalPinsListener = null;
    }
    return;
  }
  if (!globalPinsListener) {
    globalPinsListener = (e) => {
      let channel = e.subject;
      if (!(channel instanceof Ci.nsIHttpChannel)) return;
      try {
        checkChannel(channel, globalPins, true);
      } catch (ex) {
        console.log(ex);
        if (channel.loadFlags & DOCUMENT_LOAD_FLAGS) {
          channel.cancel(NS_BINDING_ABORTED);
          require('./remote-script').inChannel(channel,`
              docShell.loadURI(
                "about:certerror?e=nssBadCert&u=${encodeURIComponent(channel.URI.prePath)}&d=Tails%20Download%20and%20Verify%20Extension:%0D%0APINNING%20MISMATCH",
                1 << 16 | 1, null, null, null);
                `);
        } else {
          channel.cancel(NS_BINDING_ABORTED);
          throw ex;
        }
      }
    };
  }
  for (let e of httpEvents) events.on(e, globalPinsListener);
}

var pinnedFetch = (pins) => (...args) => fetch(pins, ...args);

Object.assign(exports,  { fetch, pinnedFetch, checkChannel, pinGlobally });
