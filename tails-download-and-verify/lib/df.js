var rxs = {
  url: /\burl:\s*(https?:\/\/\S+)\b/,
  size: /\bsize:\s*(\d+)\b/,
  hash: /\bsha256:\s*([a-f0-9]{64})\b/,
  version: /\bversion:\s*['"]([0-9.]+)['"]/
};
exports.parse = (data) => {
  data = data.replace(/^[^'"]*#.*/gm, ''); // remove most comments
  let df = {};
  for (let p in rxs) {
    let m = data.match(rxs[p]);
    if (!m) return null;
    df[p] = m[1];
  }
 return df;
};
