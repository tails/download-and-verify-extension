const { Ci } = require("chrome");
exports.inChannel = (channel, script) => {
  channel.notificationCallbacks.getInterface(Ci.nsILoadContext).topFrameElement.messageManager.loadFrameScript(
    "data:text/javascript," + encodeURIComponent(script), false
  );
};
