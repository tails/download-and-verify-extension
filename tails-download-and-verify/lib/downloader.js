const { Ci, Cc, Cu, components } = require("chrome");
Cu.import("resource://gre/modules/Downloads.jsm");
Cu.import("resource://gre/modules/Task.jsm");

const listeners = new Set();

var blob = {
  url: null,
  hash: null,
  size: 0,
  version: 0,
};

var status;
var downloadList, downloadView, curDownload;
var lastPrompt = "Download to...";
var knownDownloads = new WeakSet();
var cachedDescriptor = null;

function file(path) {
  let f = Cc["@mozilla.org/file/local;1"].createInstance(Ci.nsILocalFile);
  f.initWithPath(path);
  return f;
}

function patchSaver(saver) {
  try {
    console.log("Patching the download manager...");
    let source = saver.execute.toSource();
    let patched = source.replace(
      /(\n\s*)(channel\.notificationCallbacks = [\s\S]+?let currentBytes = )([\s\S]+\bonStopRequest: [\s\S]*\b[\s\S]*\baRequest\b[\s\S]+\baStatusCode\b[\s\S]+\btry \{\s*)([\s\S]+\)\s*\{\s*)(backgroundFileSaver\.onDataAvailable\([^;]+;)/,
      `$1let bytesRead = 0;
       $1$2bytesRead = $3
       if (aRequest instanceof Ci.nsIHttpChannel && aRequest.contentLength && Components.isSuccessCode(aStatusCode) && bytesRead < aRequest.contentLength) {
         aStatusCode = Cr.NS_BINDING_ABORTED;
         download.cancel();
         try { Cu.reportError(aRequest.name + " download failed: " + bytesRead + " < " + aRequest.contentLength); } catch (e) {}
      }
      $4
      try {
        $5
      } catch (e) {
        aRequest.cancel(Cr.NS_BINDING_ABORTED);
        throw e;
      }
      `
    );
    if (patched !== source) {
      try {
         saver.execute = Cu.import("resource://gre/modules/DownloadCore.jsm").eval(patched);
         console.log(`download.saver.execute patched: ${saver.execute.toSource()}`);
      } catch (e) {
        console.error("Could not patch download.saver.execute!");
        console.error(e.message);
      }
    } else {
      console.error(`Could not patch download.saver.execute: \n${source}`);
    }
  } catch (e) {
    console.error(e.message);
  }
}

function startDownload(source, target, prompt = lastPrompt) {
  if (prompt && prompt !== lastPrompt) lastPrompt = prompt;
  let url = source.url || source.spec || source;
  if (!target) {
    let fname = source.replace(/.*\//, "");
    new (Cu.import("resource://gre/modules/DownloadLastDir.jsm").DownloadLastDir)(
        require('sdk/window/utils').getMostRecentBrowserWindow()).getFileAsync(url, file => {
          if (file) file.append(fname);
          else file = fname;
          target = require('./filepicker').promptForFile(prompt, true, file);
          if (target) startDownload(source, target);
        });
    return;
  }
  let tab = require("sdk/tabs").activeTab;
  source = { url: url };
  if (tab && require("sdk/private-browsing").isPrivate(tab)) {
    source.isPrivate = true;
  }
  Downloads.createDownload({source: source, target: { path: target.path || target, partFilePath: `${target}.part` }})
      .then(download => {
              download.tryToKeepPartialData = true;
              patchSaver(download.saver);
              if (downloadList) downloadList.add(download);
              download.start().then(()=> checkDownload(download), error => fail(error));
              checkDownload(download);
            },
            error => fail(error)
      );
}

function tailsReconnectionFailure(download) {
  if (!download.succeeded || download.target.size == download.totalBytes) return false;
  console.log(`Tails reconnection failure? ${download.target.size} <> ${download.totalBytes}`);
  if (downloadList) downloadList.remove(download);
  let target = download.target;
  if (target.size > 0) {
    try {
      let f = file(target.path);
      let targetFile = f.clone();
      const permissions = parseInt("700", 8);
      try {
         f.permissions = permissions;
      } catch (e) {
        f.createUnique(f.NORMAL_FILE_TYPE, permissions);
      }
      try {
        targetFile.copyTo(null, `${targetFile.leafName}.part`);
      } catch (e) {
        console.error(e);
      }
      target = f;
    } catch (e) {
      console.error(e);
      target = null;
    }
  } else {
    target = null;
  }
  startDownload(download.source, target && target.path);
  return true;
}

// Return another, random mirror download URL & exclude the current mirror.
function tryAnotherMirror(download, fallback_download_url_prefix=blob.fallback_download_url_prefix, url_suffix=blob.url_suffix, mirrors=blob.mirrors) {
  if (! download.source.url.endsWith(url_suffix)) {
    throw new Error(`${download.source.url} must end with ${url_suffix}`);
  }
  let current_mirror = compareUrlToMirrors(download.source.url, mirrors, url_suffix);
  if (current_mirror !== false) {
    delete mirrors.mirrors[current_mirror];
  }
  blob.url = download.source.url = require('./mirror-dispatcher').transformURL(
    fallback_download_url_prefix + url_suffix,
    fallback_download_url_prefix,
    mirrors
  );
  console.log("Trying to resume download from another mirror: " + download.source.url);
  return download;
}

// Return the index, in the mirror list, of the mirror corresponding to a given
// URL, if one is found. Else, return false.
function compareUrlToMirrors(testurl, mirrors, url_suffix=blob.url_suffix) {
  if (! testurl.endsWith(url_suffix)) {
    throw new Error(`${testurl} must end with ${url_suffix}`);
  }
  console.log("Tested URL to compare against mirrors: " + testurl);
  for (let m_url = 0; m_url < mirrors.length; m_url++) {
    console.log("Mirror URL to compare against " + mirrors.mirrors[m_url].url_prefix + url_suffix);
    if (mirrors.mirrors[m_url].url_prefix + url_suffix == testurl && mirrors.mirrors[m_url].weight > 0) {
      console.log("Download URL corresponds to a known active mirror: " + mirrors.mirrors[m_url].url_prefix);
      return m_url; // just return the index.
    }
  }
  return false;
}

function checkDownload(download, batch) {

  if (!blob.url || /^verif/.test(status.phase) && !status.failed) return false;

  if (download === curDownload) return true;
  console.log(`checkDownload: ${curDownload}
    ${download.source.url}/${blob.url}
    ${download.currentBytes}, ${download.stopped}, ${download.error}
    `);

  if (download.source.url !== blob.url ||
    download.currentBytes === 0 && download.stopped && !download.error) return false;
  if (curDownload) {
    if (curDownload.canceled && !download.canceled ||
       curDownload.stopped && !download.stopped ||
        curDownload.endTime > download.endTime
       ) return false;
  }
  if (!knownDownloads.has(download)) {
    knownDownloads.add(download);

    download.whenSucceeded().then(
      () => {
        if (downloadList && download === curDownload) {
          console.log(`Succeeded: ${download.succeeded}, totalBytes: ${download.totalBytes}, currentBytes: ${download.currentBytes}`);
          if (tailsReconnectionFailure(download)) return;
          notify({phase: 'downloaded'});
          if (download.saver && download.saver.getSha256Hash) {
            try {
              let hash = require('./hex').bin2hex(download.saver.getSha256Hash());
              let expectedSize = blob.size;
              Verify.quick(hash, blob.hash, download.target, expectedSize);
              return;
            } catch(e) {
              console.error(e);
            }
          }
          api.verify({ path: download.target.path });
        }
      },
      (e) => fail(e)
    );
  }

  console.log("Swapping download ", curDownload, download);
  curDownload = download;

  status.phase = 'downloading';
  if (!batch) notify();
  return true;
}

function checkDownloadList(mustNotify) {
  if (!downloadList) return;
  let ret = false;
  for (let download of yield downloadList.getAll()) if (checkDownload(download, true)) ret = true;
  if (ret || mustNotify) notify();
}

Task.spawn(function () {

  downloadList = yield Downloads.getList(Downloads.ALL);

  downloadView = {
    onDownloadAdded: download => {
      if (checkDownload(download))
       console.log("Added", download);
    },
    onDownloadChanged: download => {
       if (checkDownload(download)) notify();
    },
    onDownloadRemoved: download => {
      if (download === curDownload) {
         console.log("Removed", download);
         curDownload = null;
         status.phase = "ready";
         checkDownloadList(true);
      }
    }
  };

  yield downloadList.addView(downloadView);
  checkDownloadList();
}).then(null, (e) => console.error(e));


var Verify = {
  failures: new Map(),
  info: {
    progress: 0,
    path: null,
    hash: null,
    computedHash: null,
  },
  start(info) {
    Object.assign(this.info, info);
    status.phase = 'verifying';
    status.failed = false;
  },
  get working() { return status.phase === 'verifying' },
  get done() { return status.phase === 'verified' },
  onProgressUnclamped(read, total) {
    if (!Verify.working) return;

    let percent = Verify.info.progress = Math.round(read / total * 100);
    console.log(`${percent}%`);
    notify();
  },
  onProgress: require('./clamp').clamp((read, total) => Verify.onProgressUnclamped(read, total)),
  onError(e) { if (this.working) this.fail(e); },
  onSuccess(res) {
    try {
      if (!this.working) return;
      let knownHash = this.info.hash;
      this.info.computedHash = res.hash;
      let ok = knownHash === res.hash;
      console.log(`${ok ? "MATCH!" : "FAIL!"}\n${this.info.path}\nknown: ${knownHash}\nfound: ${res.hash}\n${Math.round(res.size / 10000)}MB processed in ${res.elapsed}ms`);
      if (ok) {
        status.phase = 'verified';
        this.failures.set(this.info.hash, 0);
        notify();
      } else {
        this.fail(new Error(`Hash mismatch for "${this.info.path}"!\nknown: ${knownHash}\nfound: ${res.hash}`));
      }

    } catch (e) {
      fail(e);
    }
  },
  fail(e) {
   let failures = this.info.failures = (this.failures.get(this.info.hash) || 0) + 1;
   this.failures.set(this.info.hash, failures);
   fail(e);
  },
  quick: function(computedHash, knownHash, target, expectedSize) {
    let { size, path } = target;
    console.log(`Verify.quick -- computed: "${computedHash}", known: "${knownHash}", size: ${size} (${typeof size}, expected: ${expectedSize} (${typeof expectedSize})`);
    this.start({hash: knownHash, path});
    this.onProgressUnclamped(size, expectedSize);
    if (size != expectedSize) this.onError(new Error(`Size mismatch: expected ${expectedSize} bytes, downloaded ${size}`));
    else this.onSuccess({hash: computedHash});
  }
};


function reset(phase = 'init') {
  status = {
    downloadInfo: null,
    blob: blob,
    phase: phase, // init, ready, started, downloading, downloaded, verifying, verified
    verifyInfo: Verify.info,
    failed: false,
    error: null,
  };
}

var Config = require("../conf.json");

var fetch = require("./cert-pinner").pinnedFetch(Config.pins);

function fail(e) {
  console.error(e);
  status.failed = true;
  status.error = e && e.message || e;
  notify();
}

function selectDescriptor(contentURL) {
  if (contentURL && Config.descriptors && typeof Config.descriptors === "object") {
    let descriptors = Config.descriptors;
    for (let p in descriptors) {
      try {
        if (new RegExp(p).test(contentURL)) return descriptors[p];
      } catch (e) {
        console.log(e);
      }
    }
  }
  return Config.descriptor;
}

function setup(contentURL) {
  var fallback_download_url_prefix = blob.fallback_download_url_prefix = Config.fallback_download_url_prefix;
  var mirrors;
  var mirror_pool_url = Config.mirror_pool_url;

  if (!fallback_download_url_prefix) fail("No fallback download URL prefix configured.");
  if (!mirror_pool_url) fail("No mirror pool URL configured.");

  fetch(mirror_pool_url, data => {
    mirrors = JSON.parse(data);
    console.log("Fetched mirrors: ", mirrors);

    if (!cachedDescriptor) reset();
    let url = selectDescriptor(contentURL);
    if (!url) fail(`No download descriptor available for page ${contentURL}!`);
    else fetch(url, data => {
      let df = require('./df').parse(data);
      if (!df) fail(`Could not parse ${url}\n${data}`);
      else {
        if (data !== cachedDescriptor || status.phase === 'init') {
          reset();
          cachedDescriptor = data;
          Object.assign(blob, df);

          if (! blob.url.startsWith(fallback_download_url_prefix)) {
            throw new Error(`${blob.url} must start with ${fallback_download_url_prefix}`);
          }
          // Store url_suffix in blob, tryAnotherMirror needs it
          blob.url_suffix = blob.url.substr(fallback_download_url_prefix.length)

          // instead of using the URL from the YAML, we want to use a URL from our new mirror pool
          console.log("Unmodified download URL: ", blob.url);
          blob.url = require('./mirror-dispatcher').transformURL(blob.url, fallback_download_url_prefix, mirrors);
          console.log("Modified download URL: ", blob.url);

          // Assigning mirrors to blob so that we can later use them to check against existing downloads. Mirrors are stored as JSON array.
          blob.mirrors = mirrors;
        }

        // check if any of the mirror urls matches with curDownload.source.url
        // if yes, assign curDownload.source.url to blob.url so that we can continue the download.
        if (blob.mirrors) {
          if (curDownload && compareUrlToMirrors(curDownload.source.url, blob.mirrors, blob.url_suffix) !== false) {
            blob.url = curDownload.source.url;
          }
        }

        if (!curDownload || curDownload.source.url !== blob.url) {
          curDownload = null;
          status.phase = 'ready';
        }
        checkDownloadList();
        if (curDownload && status.phase === 'init') status.phase = 'started';
        console.log(`Setup ${curDownload} phase ${status.phase}`);
        notify();
      }
    }, fail);
  }, fail);
}

function notify(statusChanges) {

  if (statusChanges) Object.assign(status, statusChanges);
  if (curDownload && /^download/.test(status.phase)) {
    status.downloadInfo = {
      url: curDownload.source.url,
      path: curDownload.target.path,
      size: curDownload.target.size,
      totalBytes: curDownload.totalBytes,
      startTime: curDownload.startTime.getTime(),
      currentBytes: curDownload.currentBytes,
      progress: curDownload.progress,
      hasProgress: curDownload.hasProgress,
      stopped: curDownload.stopped,
      canceled: curDownload.canceled,
      succeeded: curDownload.succeeded,
      error: curDownload.error
    };
  }
  for (let l of listeners.values()) l(status);
}

reset();

var api = {

  addListener(listener) {
    listeners.add(listener);
  },
  removeListener(listener) {
    listeners.delete(listener);
  },

  pageLoaded(contentURL) {
    console.log(`pageLoaded ${contentURL}, phase ${status.phase}, failed ${status.failed}`);
    if (status.failed && !(curDownload && curDownload.canceled)) reset();
    if (status.phase !== 'init') notify();
    setup(contentURL);
  },

  start(prompt, path) {
    reset(status.phase);
    startDownload(blob.url, path, prompt);
  },

  retry() {
    if (curDownload) {
      let { source, target } = curDownload;
      if (!tailsReconnectionFailure(curDownload)) {
        this.cancel();
        this.start(source, target.path);
      }
    } else {
      this.start();
    }
  },

  resume() {
    reset(status.phase);
    if (curDownload) {
      if (curDownload.error && curDownload.error.becauseSourceFailed) {
        curDownload = tryAnotherMirror(curDownload);
      }
      curDownload.start();
    }
  },
  pause() {
    if (curDownload) {
      curDownload.cancel();
    }
  },
  cancel() {
    try {
      console.log("Entering cancel");
      status.phase = 'started';
      notify();
      let d = curDownload;
      if (d) {
        d.finalize(true);
        if (downloadList) {
          try {
            downloadList.remove(d);
          } catch (e) {
            console.error(e);
          }
        }
        notify();
      }
      console.log("Done cancel");
    } finally {
      console.log("Exiting cancel");
    }
  },

  verify(opts = {}) {
    let path = opts.path || require('./filepicker').promptForFile(opts.prompt || "Select blob file to verify");
    if (!path) return;
    let hash = opts.hash || blob.hash;
    Verify.start({
      path,
      hash,
      progress: 0,
      computedHash: null,
    });
    try {
      require("./hash").sha256(path, true, opts.onProgress || Verify.onProgress,
          opts.size || (hash === blob.hash ? parseInt(blob.size) : 0)
        ).then(opts.onSuccess || Verify.onSuccess.bind(Verify), opts.onError || Verify.onError.bind(Verify));
    } catch(e) { fail(e); }
  },

  reveal(path) {
    try {
      file(path).reveal();
    } catch (e) {
      console.error(e);
    }
  },

  shutdown() {
    if (downloadList) {
      downloadList.removeView(downloadView);
      downloadList = downloadView = null;
    }
  },
  
  reset() {
    this.cancel();
    reset();
    notify();
  }
};

Object.assign(exports, api);
