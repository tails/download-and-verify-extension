/*
@licstart
Copyright (C) 2015-2016 Tails Developers

    The JavaScript code in this page is free software: you can
    redistribute it and/or modify it under the terms of the GNU
    General Public License (GNU GPL) as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.  The code is distributed WITHOUT ANY WARRANTY;
    without even the implied warranty of MERCHANTABILITY or FITNESS
    FOR A PARTICULAR PURPOSE.  See the GNU GPL for more details.

    As additional permission under GNU GPL version 3 section 7, you
    may distribute non-source (e.g., minimized or compacted) forms of
    that code without the copy of the GNU GPL normally requestuired by
    section 4, provided you include this license notice and a URL
    through which recipients can access the Corresponding Source.
@licend
*/

'use strict';

/*
 * To enable console logging in production set DEBUG to true
 */

var DEBUG = false;
// Allow debugging through the JS console
if(DEBUG === false) {
    var console = { log: function() {} };
}

/*
 * Some versions of IE do not support logging to the console.
 * When we set ALERTFALLBACK to true, we can log to alert.
 * In any case, if console function does not exist, we do not want to log
 * nor throw an error, thus disabling console.log.
 */

var ALERTFALLBACK = false;
if (typeof console === "undefined" || typeof console.log === "undefined") {
  console = {};
  if (ALERTFALLBACK) {
    console.log = function(msg) {
      alert(msg);
    };
  } else {
    console.log = function() {};
  }
}

function get(path, callback) {

  var max_expected_filelength = 8 * Math.pow(2,10);

  // Do the usual request stuff
  var request = new XMLHttpRequest();
  request.open('GET', path);

  // Handle network errors
  request.onerror = function() {
    console.log("Network Error");
    return false;
  };

  request.onreadystatechange = function() {
    // Check the status
    // 4 = Headers and responseText have been loaded.
    if (request.readyState === 4) {
      // console.log(request.getAllResponseHeaders());
      // check filesize is not too big
      if(request.responseText.length > max_expected_filelength) {
        console.log("Retrieved content too big. Response length: " + request.responseText.length + " max_expected_filelength: " +  max_expected_filelength);
        request.abort();
        return false;
      }
      // Check TLS
      if(!/^https:\/\//.test(request.responseURL)) {
        console.log("File not served over TLS.");
        request.abort();
        return false;
      }
      // empty string = text responseType
      if(request.responseType != "") {
        console.log("Expected responseType is text.");
        request.abort();
        return false;
      }

      // 200 = The request has succeeded.
      if (request.status === 200) {
        var data = request.responseText;
        console.log("data = request.responseText: " + data);
        if (callback) {
          callback(data);
        }
      } else {
        console.log(request.statusText);
        request.abort();
        return false;
      }
    }
  };

  // Make the request
  request.send();
}

/*
 * Choose a random mirror, based on weight.
 * weight="0" - this mirror is not active.
 * There is no max weight.
 * @params: JSON mirror data, formatted like this:
   - schema: https://git-tails.immerda.ch/mirror-pool/tree/schema.json
   - example: https://git-tails.immerda.ch/mirror-pool/tree/example-mirrors.json
 * @returns  false or one randomly chosen host URL prefix
 */
function getRandomMirrorUrlPrefix(data) {
  if (data.mirrors.length > 0) {
    var end_of_last_range = 0,
    i,
    mirrors = [],
    picked_value,
    ranges_end = [];

    // Build a "compressed" version of the array that would contain N times each
    // mirror, with N being its weight. We "compress" it by only storing, for
    // each mirror, the index of the last element it would occupy in the
    // uncompressed version of the array.
    for (i = 0; i < data.mirrors.length; i++) {
      ranges_end[i]
      = end_of_last_range
      = end_of_last_range + data.mirrors[i].weight;
    }

    // If we were using the "uncompressed" version of the aforementioned array,
    // we would do something like Math.random() * uncompressed_array.length,
    // and the result would be the mirror we want. But here we are using
    // a "compressed" version of this array, so the result we get instead is
    // the minimum *weight* of the mirror we pick randomly.
    picked_value = Math.floor(Math.random() * end_of_last_range) + 1;

    // Pick the first mirror that would occupy a range that ends at or after
    // uncompressed_array[picked_value] in the "uncompressed" version
    // of the array. I.e. once converted to the "compressed" optimization:
    // the one whose ranges_end[i] is bigger or equal to picked_value.
    for (i = 0; i < data.mirrors.length; i++) {
      if (picked_value <= ranges_end[i]) {
        if(isValidURL(data.mirrors[i].url_prefix)) {
          return data.mirrors[i].url_prefix;
        }
      }
    }
  } else {
    return false;
  }
}

/*
 * Returns true if url is a valid URL pattern
 */
function isValidURL(url) {
  var url_pattern = new RegExp('^(http|https):\/\/[a-z0-9\-_]+(\.[a-z0-9\-_]+)+([a-z0-9\-_\.,@\?^=%&;:/~\+#]*[a-z0-9\-\_#@\?^=%&;/~\+])?$', 'i');
  var test = url_pattern.test(url)
  console.log("Tested URL: " + url + " " + test);
  return(test);
}

/*
 * This is the main function call, to be called by our website
 */
function replaceUrlPrefixWithRandomMirror(linkelements) {
  var data, index, max_expected_linkelements, url_current, fallback_download_url_prefix, url_new, url_prefix;

  max_expected_linkelements = 15;
  if(linkelements.length > max_expected_linkelements) {
    console.log(linkelements.length + " exceeds number of expected elements: " + max_expected_linkelements);
    return false;
  }

  fallback_download_url_prefix = "http://dl.amnesia.boum.org/tails/";

  return get('/mirrors.json', function(data){
     if( data !== "undefined" ) {
       data = JSON.parse(data);

       // pick a random mirror
       url_prefix = getRandomMirrorUrlPrefix(data);
       console.log("Random URL prefix: " + url_prefix);

       if(url_prefix) {
         // replace the default prefix with the newly picked one,
         // in the href of each linkelement
        for (index = 0; index < linkelements.length; index++) {
          // remove possible URL whitespaces and line breaks as created by ikiwiki
          url_current = linkelements[index].getAttribute('href');
          url_current = url_current.trim();
          if (url_current !== 'undefined' && isValidURL(url_current)) {
            url_new = url_prefix +
            url_current.slice(fallback_download_url_prefix.length);
            console.log("index: "+ index + " url_current: " + url_current + " --> url_new:" + url_new);
            linkelements[index].setAttribute('href', url_new)
          } else {
            console.log(url_current + " appears to be malformed.");
          }
        }
      } else {
        console.log("Could not determine url_prefix.");
      }
    }
  });
}

/* This function is used by DAVE instead of
 * replaceUrlPrefixWithRandomMirror().
 * Use mirror pool configuration and select a random mirror.
 */
function transformURL(url, fallback_download_url_prefix, mirrors) {
  return getRandomMirrorUrlPrefix(mirrors) +
    url.slice(fallback_download_url_prefix.length);
}

/*
 * Assign transformURL to exports, so that it can be used
 * by tails-transform-mirror-url via nodejs.
 * Will return target object.
 */

if(typeof exports !== 'undefined') {
  // Now we know that we're not running in the browser
  module.exports.transformURL = transformURL;
}
