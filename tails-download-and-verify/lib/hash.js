const { Ci, Cc, Cu, components } = require("chrome");

Cu.import("resource://gre/modules/osfile.jsm");
Cu.import("resource://gre/modules/Task.jsm");

var sha256 = (path, hex, onProgress, expectedSize = 0) => Task.spawn(function () {

  const CHUNK_SIZE = 8 * 1024 * 1024;
  const FORCE_GC_AT = 400 * 1024 * 1024;
  let hasher = Cc["@mozilla.org/security/hash;1"]
                 .createInstance(Ci.nsICryptoHash);
  hasher.init(hasher.SHA256);

  const startTime = Date.now();
  let file = yield OS.File.open(path);
  try {
    let size = (yield file.stat()).size;
    if (expectedSize !== 0 && size !== expectedSize) throw new Error("Wrong size!");
    yield file.setPosition(0, OS.File.POS_START);
    let total = 0;
    let maxJank = 0;
    for (let bytesRead, allocated = 0;;) {
      let buffer = yield file.read(CHUNK_SIZE);
      if ((bytesRead = buffer.length) === 0) break;
      let t = Date.now();
      if ((allocated += bytesRead) >= FORCE_GC_AT) {
         Cu.forceGC();
        allocated = 0;
      }

      total += bytesRead;
      hasher.update(buffer, bytesRead);

      let elapsed = Date.now() - t;
      if (elapsed > maxJank) maxJank = elapsed;
      if (onProgress) onProgress(total, size);
    }
    console.log(`maxJank: ${maxJank}`);

    let hash = hex ? require('./hex').bin2hex(hasher.finish(false)) : hasher.finish(true);

    throw new Task.Result({hash, size: total, startTime, elapsed: Date.now() - startTime, path, jank: maxJank });
  } finally {
    yield file.close();
    Cu.forceGC();
  }
});


/*
// Sample code
{
  let path = "/home/giorgio/iso/tails-i386-1.7.iso";
  let hash = "372ba423c46ce76393f5e8cd64136e5e9c4a806def993951fe8dbf7d93be2546"

  let onProgress = require('clamp').clamp((read, total) => console.log(Math.round(read/total * 100)) + "%");
  sha256(path, true, onProgress)
    .then(
     (res) => {
       console.log(`${hash === res.hash ? "MATCH!" : "FAIL!"}\nknown: ${hash}\nfound: ${res.hash}\n${Math.round(res.size / 10000)}MB processed in ${res.elapsed}ms`);
     },
     (exception) => {
      console.log(exception)
     }
  );
}
*/

Object.assign(exports, { sha256 });
