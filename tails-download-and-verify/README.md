#Tails Download and Verify
A browser extension to download and verify Tails ISO images

##Compiling

Copy the latest version of
<https://git-tails.immerda.ch/mirror-pool-dispatcher/tree/lib/js/mirror-dispatcher.js>
to `lib/`.

Install [JPM](https://developer.mozilla.org/en-US/Add-ons/SDK/Tools/jpm) and
use `jpm xpi` on the command line to build just the extension, or
`/build.sh` in the parent directory to synchronize the www/ directory as well.

##Customization
In the `conf.json` the following properties can be configured:
* `descriptor` - URL referencing the YAML IDF for the latest ISO release
* `certs` - key-value pairs mapping each domain to one SSL certificates that
  should be considered 'safe' (an in-house version of certificate pinning)
