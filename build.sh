#!/bin/bash
base=$(dirname "$0")
dir="$base/tails-download-and-verify"
mirror_dispatcher_url='https://git-tails.immerda.ch/mirror-pool-dispatcher/plain/lib/js/mirror-dispatcher.js'
mirror_dispatcher_file="lib/mirror-dispatcher.js"
pushd "$dir" >/dev/null 2>&1 || (
  echo >&2 "FATAL: Channot chdir to $dir."
  exit 1
)
echo -n "Removing old XPIs... "
rm -f *.xpi >/dev/null && echo "done."
ver=$(egrep '"version": "[0-9]+\.[^"]+"' package.json | sed -re 's/.*"([0-9]+\.[^"]+)".*/\1/')

if [ -f "$mirror_dispatcher_file" ]; then
  echo "$mirror_dispatcher_file exists, won't download it (erase the local file to update from the $mirror_dispatcher_url)."
else
  echo "Importing $mirror_dispatcher_file from $mirror_dispatcher_url ..."
  curl \
  --proto -all,https \
  --tlsv1 \
  --output "$mirror_dispatcher_file" \
  "$mirror_dispatcher_url"
fi
echo "Building extension version $ver"
jpm xpi || exit 2
popd
echo "Copying "*.xpi" to $base/www/dave.xpi..."
cp "$dir/"*.xpi "$base/www/dave.xpi" >/dev/null
ver_page="$base/www/download.html"
ver_line=$(egrep '<[a-z][^>]+id="extension-version"[^>]*>[0-9]' $ver_page)
if [ "$ver_line" ] && ! echo "$ver_line" | grep -F "$ver" ; then
  echo "Updating download.html to version $ver..."
  sed -re 's/(<[a-z][^>]+id="extension-version"[^>]*>)[0-9\.rcba]+/\1'$ver'/' $ver_page > $ver_page.upd && mv $ver_page.upd $ver_page
fi
echo "DONE."
